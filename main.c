#include <stdio.h>
#include <string.h>
// Wprowadź napis nr 1
// Wprowadź napis nr 2
// Wyświetl oba napisy
// Wyświetl długości obu napisów
// Dołącz napis nr 2 do napisu nr 1
// Przepisz napis nr 2 do napisu nr 1
// Sprawdź, czy napisy są takie same
// Wyświetl napisy w kolejności alfabetycznej
// Oceny: 4 na 3, po pół oceny w górę za każdy dodatkowy punkt; na 6 wersja z "dowolną" ilością napisów 

char arrc[10][20];


int counter=0;

void input_string(int n){
    int tmp=counter;
    char input[20];
    for(int i=counter;i<tmp+n;i++){
        //getchar();
        scanf(" %[^\n]s",arrc[i]);
        counter++;
    }
}
void print_strings(){
    for(int i=0;i<counter;i++){
        printf("%s\n",arrc[i]);
    }
}

int print_length_strings(){
    for(int i=0;i<counter;i++){
        printf("indeks = %d ma dlusgosc %d\n",i,strlen(arrc[i]));
    }
}

void merge_strings(int a,int b){
    strcat(arrc[a],arrc[b]);

}

void compare_strings(int a,int b){
    if(strcmp(arrc[a],arrc[b])==0){
        printf("Napisy sa takie same \n");
    }else{
        printf("Napisy roznia sie \n");
    }
}

void print_strings_in_alphabetical_order(char tab[][20],int size){
    char result[10][20];
    char tmp[20];
    for(int i=0;i<size;i++){
        strcpy(result[i],tab[i]);
    }

    for(int i=0;i<size;i++){
        for(int j=i+1;j<size;j++)
            if(strcmp(result[i],result[j])>0){
                strcpy(tmp,result[i]);
                strcpy(result[i],result[j]);
                strcpy(result[j],tmp);
            }
    }

     for(int i=0;i<size;i++){
        printf("%s\n",result[i]);
    }

}

void sort_string_array(char tab[][20],int size){
    char tmp[20];

     for(int i=0;i<size;i++){
        for(int j=i+1;j<size;j++)
            if(strcmp(tab[i],tab[j])>0){
                strcpy(tmp,tab[i]);
                strcpy(tab[i],tab[j]);
                strcpy(tab[j],tmp);
            }
    }

}

int menu(){
  
    int liczba;
    printf("\n");
    printf("Wybierz opcje \n");
    printf(" \n");

    printf("1. Dodaj napisy \n");
    printf("2. Wyświetl napisy \n");
    printf("3. Wyswietl dlugosci napisow  \n");
    printf("4. Dolacz napis nr 2 do nr 1 \n");
    printf("5. Przepisz napis nr 2 do napisu nr 1 \n");
    printf("6. Sprawdz czy napisy sa takie same  \n");
    printf("7. Wyswietl napisy w kolejnosci alfabetycznej \n");
    printf("8. Długosc napisu o podanym indeksie \n");
    printf("9. Sortowanie tablicy głownej \n");

   
    printf("0. Koniec Programu \n");
    printf(" \n");

    scanf("%d",&liczba);

    return liczba;
}

int main(){
    int a,b;
    int select;
        do{
        select = menu();
        switch (select)
        {
        case 1:
            printf("Podaj ile napisow chcesz dodac \n");
            scanf("%d",&a);
            input_string(a);
            break;
        case 2:
            print_strings();
            break;
        case 3:
            print_length_strings();
            break;
        case 4:
            
            printf("Podaj indeks do ktorego chcesz dodac \n");
            scanf("%d",&a);
            printf("Podaj indeks jaki chcesz dodac do pierwszego \n");
            scanf("%d",&b);
            merge_strings(a,b);

            break;
        case 5:
            printf("Podaj indeks do ktorego chcesz zapisac \n");
            scanf("%d",&a);
            printf("Podaj indeks jaki przepisac \n");
            scanf("%d",&b);
            strcpy(arrc[a], arrc[b]);
            break;
        case 6:
            printf("Podaj indeks napisu ktory chcesz sparwdzic  \n");
            scanf("%d",&a);
            printf("Podaj indeks drugiego  napisu ktorego chcesz sparwdzic  \n");
            scanf("%d",&b);
            compare_strings(a,b);

            break;
        case 7:
            print_strings_in_alphabetical_order(arrc,counter);
            break;
        case 8:
            printf("Podaj indeks \n");
            scanf("%d",&a);
            printf("indeks = %d ma dlusgosc %d\n",a,strlen(arrc[a]));
            break;
        case 9:
            sort_string_array(arrc,counter);
            break;
        }
    }while(select!=0); 
    return 0;
}